!(function(global) {
	var ArrayProto = Array.prototype;

	var arrfn = {
		every: ArrayProto.every || function(fun) {
			if (this === null)
				throw new TypeError();

			var t = Object(this),
				len = t.length >>> 0;

			if (typeof fun !== "function")
				throw new TypeError();

			var thisp = arguments[1];
			for (var i = 0; i < len; i++) {
				var val = t[i];
				if (val !== undefined && !fun.call(thisp, t[i], i, t))
					return false;
			}

			return true;
		},
		forEach: ArrayProto.forEach || function(fun, thisArg) {
			if (this === null)
				throw new TypeError();

			var t = Object(this),
				len = t.length >>> 0;

			if (typeof fun !== "function")
				throw new TypeError();

			var thisp = arguments[1];
			for (var i = 0; i < len; i++) {
				var val = t[i];
				if (val !== undefined)
					fun.call(thisArg, val, i, t);
			}
		},
		filter: ArrayProto.filter || function(fun) {
			if (this == null)
				throw new TypeError();

			var t = Object(this),
				len = t.length >>> 0;

			if (typeof fun != "function")
				throw new TypeError();

			var res = [],
				thisp = arguments[1];

			for (var i = 0; i < len; i++) {
				var val = t[i];
				if (fun.call(thisp, val, i, t))
					res.push(val);
			}

			return res;
		},
		map: Array.prototype.map || function(fun, thisArg) {
			var T,
				A;

			if (this == null)
				throw new TypeError("this is null or not defined");

			var O = Object(this);
			var len = O.length >>> 0;

			if (typeof fun != "function")
				throw new TypeError();

			if (thisArg)
				T = thisArg;

			A = new Array(len);

			for (var i = 0; i < len; i++) {
				var kValue,
					mappedValue;

				kValue = O[i];
				mappedValue = fun.call(T, kValue, i, O);
				A[i] = mappedValue;
			}
			return A;
		},
		remove: function(v) {
			var len = this.length;
			for (var i = 0; i < len; i++)
				if (this[i] === v) {
					this.splice(i, 1);
					return true;
				}
			return false;
		},
		reduce: function(fun) {
			if (this === void 0 || this === null)
				throw new TypeError();

			var t = Object(this),
				len = t.length >>> 0,
				k = 0,
				accumulator;

			if (typeof fun != 'function')
				throw new TypeError();
			if (len == 0 && arguments.length == 1)
				throw new TypeError();

			if (arguments.length >= 2)
				accumulator = arguments[1];
			else
				do {
					if (k in t) {
						accumulator = t[k++];
						break;
					}
					if (++k >= len)
						throw new TypeError();
				} while (true);
			while (k < len) {
				if (k in t)
					accumulator = fun.call(undefined, accumulator, t[k], k, t);
				k++;
			}
			return accumulator;
		},
		indexOf: function(searchElement) {
			if (this == null) {
				throw new TypeError();
			}
			var t = Object(this);
			var len = t.length >>> 0;
			if (len === 0) {
				return -1;
			}
			var n = 0;
			if (arguments.length > 1) {
				n = Number(arguments[1]);
				if (n != n) { // shortcut for verifying if it's NaN
					n = 0;
				} else if (n != 0 && n != Infinity && n != -Infinity) {
					n = (n > 0 || -1) * Math.floor(Math.abs(n));
				}
			}
			if (n >= len) {
				return -1;
			}
			var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
			for (; k < len; k++) {
				if (k in t && t[k] === searchElement) {
					return k;
				}
			}
			return -1;
		},
		clone: function() {
			return this.slice(0);
		}
	};

	for (var fn in arrfn) {
		if (arrfn.hasOwnProperty(fn) && !ArrayProto.hasOwnProperty(fn)) {
			ArrayProto[fn] = arrfn[fn];
		}
	}

	if (!String.prototype.trim) {
		String.prototype.trim = function() {
			return this.replace(new RegExp('(^\\s*)|(\\s*$)', 'g'), "");
		};
	}

	var trimLeft = /^\s+/,
		trimRight = /\s+$/,
		rnotwhite = /\S/;

	if (rnotwhite.test("\xA0")) {
		trimLeft = /^[\s\xA0]+/;
		trimRight = /[\s\xA0]+$/;
	}

	if (!String.prototype.trim) {
		String.prototype.trim = function() {
			return this.replace(new RegExp('(^\\s*)|(\\s*$)', 'g'), "");
		};
	}

	if (!String.prototype.trimLeft) {
		String.prototype.trimLeft = function() {
			return this.replace(trimLeft, "");
		};
	}

	if (!String.prototype.trimRight) {
		String.prototype.trimRight = function() {
			return this.replace(trimRight, "");
		};
	}

	Error.captureStackTrace = Error.captureStackTrace || function (obj) {
		if (Error.prepareStackTrace) {
			var frame = {
				isEval: function () { return false; },
				getFileName: function () { return "filename"; },
				getLineNumber: function () { return 1; },
				getColumnNumber: function () { return 1; },
				getFunctionName: function () { return "functionName" }
			};

			obj.stack = Error.prepareStackTrace(obj, [frame, frame, frame]);
		} else {
			obj.stack = obj.stack || obj.name || "Error";
		}
	};

	if (!Object.keys) {
		Object.keys = (function() {
			var hasOwnProperty = Object.prototype.hasOwnProperty,
				hasDontEnumBug = !({
					toString: null
				}).propertyIsEnumerable('toString'),
				dontEnums = [
					'toString', 'toLocaleString', 'valueOf', 'hasOwnProperty',
					'isPrototypeOf', 'propertyIsEnumerable', 'constructor'
				],
				dontEnumsLength = dontEnums.length;

			return function(obj) {
				if (typeof obj !== 'object' && typeof obj !== 'function' || obj === null)
					throw new TypeError('Object.keys called on non-object');

				var result = [];

				for (var prop in obj) {
					if (hasOwnProperty.call(obj, prop))
						result.push(prop);
				}

				if (hasDontEnumBug) {
					for (var i = 0; i < dontEnumsLength; i++) {
						if (hasOwnProperty.call(obj, dontEnums[i]))
							result.push(dontEnums[i]);
					}
				}
				return result;
			}
		})()
	}

})(window);

!(function(global) {
	var ape = global.ape = function() {

	};

	function getType(object) {
		var _t;
		return ((_t = typeof(object)) == "object" ? object == null && "null" ||
		Object.prototype.toString.call(object).slice(8, -1) : _t).toLowerCase();
	}

	ape.extend = function(o1, o2) {
		if (getType(o2) === 'object')
			for (var k in o2) {
				if (o2.hasOwnProperty(k)) o1[k] = o2[k];
			}
		else {
			throw new TypeError("o1 or o2 must be Object!");
		}
		return o1;
	};

	ape.extend(ape, {
		isObject: function(a) {
			return getType(a) === "object";
		},
		isArray: function(a) {
			return getType(a) === 'array';
		},
		isString: function(a) {
			return getType(a) === "string";
		},
		isFunction: function(a) {
			return getType(a) === "function";
		},
		isNumber: function(a) {
			return getType(a) === 'number';
		},
		isDate: function(a) {
			return getType(a) === 'date';
		},
		isNumeric: function(a) {
			return a - parseFloat(a) >= 0;
		},
		isRegExp: function(obj) {
			return !!(obj && obj.test && obj.exec && (obj.ignoreCase || obj.ignoreCase === false));
		},
		isError: function(obj) {
			return getType(obj) === 'error' || obj instanceof Error;
		},
		isEmpty: function(o) {
			for (var prop in o)
				if (o.hasOwnProperty(prop))
					return false;
			return true;
		}
	});

})(window);