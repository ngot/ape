var http = require('http');
var mq = require('mq');

(function() {
	console.log("Static Server Running On Port : 80 ...");
	new http.Server(80, http.fileHandler('../')).run();
}).start();

(function() {
	console.log("Dinymic Server Running On Port : 81 ...");
	new http.Server(81, new mq.Routing({
		'^/rpc(/.*)': function(r) {
			r.response.write('Hello, World!');
		}
	})).run();
}).start().join();