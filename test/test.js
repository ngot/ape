(function() {

	var tests = ["test/foundation_test.js"];

	var scripts = ['src/foundation.js', 'test/libs/mocha.js', 'test/libs/assert.js'];
	var css = ['test/libs/mocha.css'];
	var head = document.head || document.getElementsByTagName("head")[0] || document.body;

	if (document.body)
		load();
	else
		window.onload = load;

	function load() {
		for (var i = 0; i < css.length; i++) {
			var c = document.createElement('link');
			c.type = 'text/css';
			c.rel = 'stylesheet';
			c.href = 'http://' + location.host + '/' + css[i] + "?" + new Date().getTime();
			head.appendChild(c);
		}
		load_script(scripts, run);
	}

	function load_script(jss, fn) {
		if (jss.length > 0) {
			var c = document.createElement("script");
			c.type = "text/javascript";
			if (c.readyState) {
				c.onreadystatechange = function() {
					if (this.readyState === 'loaded') {
						setTimeout(function() {
							load_script(jss, fn);
						}, 0);
					}
				};
			} else {
				c.onload = function() {
					load_script(jss, fn);
				};
			}
			c.src = "http://" + location.host + "/" + jss.pop() + "?" + new Date().getTime();
			head.appendChild(c);
		} else {
			if (fn) fn();
		}
	}

	function run() {
		mocha.setup('bdd');
		mocha.reporter("html");
		load_script(tests, mocha.run);
	}
})();