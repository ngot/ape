describe("foundation_test", function() {

	describe("Array", function() {

		it("every", function() {
			var a = [1, 2, 3];
			assert.ok(a.every(function(c) {
				return c > 0;
			}));
		});

		it("forEach", function() {
			var a = [1, 2, 3];
			a.forEach(function(c, i) {
				a[i]++;
			});
			assert.deepEqual(a, [2, 3, 4]);
		});

		it("filter", function() {
			var a = [1, 2, 3];
			a = a.filter(function(c) {
				return c > 1;
			});
			assert.deepEqual(a, [2, 3]);
		});

		it("map", function() {
			var a = [1, 2, 3];
			a = a.map(function(c) {
				return ++c;
			});
			assert.deepEqual(a, [2, 3, 4]);
		});

		it("remove", function() {
			var a = [1, 2, 3];
			a.remove(2);
			assert.deepEqual(a, [1, 3]);

			a = [1, 2, 3];
			a.remove(3);
			assert.deepEqual(a, [1, 2]);
		});

		it("reduce", function() {
			var a = [0, 1, 2, 3, 4].reduce(function(previousValue, currentValue, index, array) {
				return previousValue + currentValue;
			}, 10);
			assert.equal(a, 20)
		});

		it("indexOf", function() {
			var a = [1, 2, 3];
			assert.equal(a.indexOf(1), 0);
			assert.equal(a.indexOf(4), -1);
		});

		it("clone", function() {
			var a = [1, 2, 3];
			var b = a.clone();
			assert.ok(!(a === b));
			assert.deepEqual(a, b);
		});

	});

	describe("String", function() {

		it("trim", function() {
			var str = " abc ";
			assert.equal("abc", str.trim());
			str = "\
			 abc \
			  ";
			assert.equal("abc", str.trim());
		});

		it("trimLeft", function() {
			var str = " abc ";
			assert.equal("abc ", str.trimLeft());
			str = " \
			abc ";
			assert.equal("abc ", str.trimLeft());
		});

		it("trimRight", function() {
			var str = " abc ";
			assert.equal(" abc", str.trimRight());
			str = " abc \
			 ";
			assert.equal(" abc", str.trimRight());
		});

	});

	describe("util", function() {

		it("extend", function() {
			assert.throws(function() {
				ape.extend(1, 2);
			});

			var o1 = {
				a: 1,
				b: 2
			};
			var o2 = {
				b: 3,
				c: 4
			};

			var o3 = {
				a: 1,
				b: 3,
				c: 4
			};

			var r = ape.extend(o1, o2);
			assert.deepEqual(r, o3);
			assert.deepEqual(o1, o3);
		});

		it("isObject", function() {
			assert.ok(!ape.isObject(new Date()));
			assert.ok(!ape.isObject(Number.NEGATIVE_INFINITY));
			assert.ok(!ape.isObject(Number.POSITIVE_INFINITY));
			assert.ok(!ape.isObject(Infinity));
			assert.ok(!ape.isObject(NaN));
			assert.ok(!ape.isObject(null));
			assert.ok(!ape.isObject(undefined));
			assert.ok(!ape.isObject(true));
			assert.ok(!ape.isObject(false));
			assert.ok(!ape.isObject(0));
			assert.ok(!ape.isObject(1));
			assert.ok(!ape.isObject(1.1));
			assert.ok(ape.isObject({}));
			assert.ok(ape.isObject(new Object()));
			assert.ok(!ape.isObject(function() {
			}));
			function a() {
			}

			assert.ok(ape.isObject(new a()));
			assert.ok(!ape.isObject(Error));
			assert.ok(!ape.isObject(Error()));
			assert.ok(!ape.isObject(new Error()));
			assert.ok(!ape.isObject([]));
			assert.ok(!ape.isObject(new Array()));
			assert.ok(!ape.isObject(Array));
			assert.ok(!ape.isObject(Array.prototype));
			assert.ok(!ape.isObject(new Number()));
			assert.ok(!ape.isObject(Number));
			assert.ok(!ape.isObject(Number.prototype));
			assert.ok(!ape.isObject(new String()));
			assert.ok(!ape.isObject(String));
			//assert.ok(!ape.isObject(String.prototype));

			assert.ok(!ape.isObject(RegExp));
			assert.ok(!ape.isObject(RegExp()));
			assert.ok(!ape.isObject(new RegExp()));
			assert.ok(!ape.isObject(RegExp.prototype));
			assert.ok(!ape.isObject(/\d/));
		});

		it("isArray", function() {
			assert.ok(ape.isArray([]));
			assert.ok(ape.isArray(new Array()));
			assert.ok(!ape.isArray(Array));
			assert.ok(ape.isArray(Array.prototype));

			assert.ok(!ape.isArray(new Date()));
			assert.ok(!ape.isArray(Number.NEGATIVE_INFINITY));
			assert.ok(!ape.isArray(Number.POSITIVE_INFINITY));
			assert.ok(!ape.isArray(Infinity));
			assert.ok(!ape.isArray(NaN));
			assert.ok(!ape.isArray(null));
			assert.ok(!ape.isArray(undefined));
			assert.ok(!ape.isArray(true));
			assert.ok(!ape.isArray(false));
			assert.ok(!ape.isArray(0));
			assert.ok(!ape.isArray(1));
			assert.ok(!ape.isArray(1.1));
			assert.ok(!ape.isArray({}));
			assert.ok(!ape.isArray(new Object()));
			assert.ok(!ape.isArray(function() {
			}));
			function a() {
			}

			assert.ok(!ape.isArray(new a()));
			assert.ok(!ape.isArray(Error));
			assert.ok(!ape.isArray(Error()));
			assert.ok(!ape.isArray(new Error()));

			assert.ok(!ape.isArray(new Number()));
			assert.ok(!ape.isArray(Number));
			assert.ok(!ape.isArray(Number.prototype));
			assert.ok(!ape.isArray(new String()));
			assert.ok(!ape.isArray(String));
			assert.ok(!ape.isArray(String.prototype));

			assert.ok(!ape.isArray(RegExp));
			assert.ok(!ape.isArray(RegExp()));
			assert.ok(!ape.isArray(new RegExp()));
			assert.ok(!ape.isArray(RegExp.prototype));
			assert.ok(!ape.isArray(/\d/));
		});

		it("isString", function() {

			assert.ok(ape.isString("string"));
			assert.ok(ape.isString(new String()));
			assert.ok(!ape.isString(String));
			//assert.ok(ape.isString(String.prototype));

			assert.ok(!ape.isString(new Date()));
			assert.ok(!ape.isString(Number.NEGATIVE_INFINITY));
			assert.ok(!ape.isString(Number.POSITIVE_INFINITY));
			assert.ok(!ape.isString(Infinity));
			assert.ok(!ape.isString(NaN));
			assert.ok(!ape.isString(null));
			assert.ok(!ape.isString(undefined));
			assert.ok(!ape.isString(true));
			assert.ok(!ape.isString(false));
			assert.ok(!ape.isString(0));
			assert.ok(!ape.isString(1));
			assert.ok(!ape.isString(1.1));
			assert.ok(!ape.isString({}));
			assert.ok(!ape.isString(new Object()));
			assert.ok(!ape.isString(function() {
			}));
			function a() {
			}

			assert.ok(!ape.isString(new a()));
			assert.ok(!ape.isString(Error));
			assert.ok(!ape.isString(Error()));
			assert.ok(!ape.isString(new Error()));

			assert.ok(!ape.isString(new Number()));
			assert.ok(!ape.isString(Number));
			assert.ok(!ape.isString(Number.prototype));

			assert.ok(!ape.isString([]));
			assert.ok(!ape.isString(new Array()));
			assert.ok(!ape.isString(Array));
			assert.ok(!ape.isString(Array.prototype));

			assert.ok(!ape.isString(RegExp));
			assert.ok(!ape.isString(RegExp()));
			assert.ok(!ape.isString(new RegExp()));
			assert.ok(!ape.isString(RegExp.prototype));
			assert.ok(!ape.isString(/\d/));
		});

		it("isFunction", function() {

			assert.ok(!ape.isFunction(new Date()));
			assert.ok(!ape.isFunction(Number.NEGATIVE_INFINITY));
			assert.ok(!ape.isFunction(Number.POSITIVE_INFINITY));
			assert.ok(!ape.isFunction(Infinity));
			assert.ok(!ape.isFunction(NaN));
			assert.ok(!ape.isFunction(null));
			assert.ok(!ape.isFunction(undefined));
			assert.ok(!ape.isFunction(true));
			assert.ok(!ape.isFunction(false));
			assert.ok(!ape.isFunction(0));
			assert.ok(!ape.isFunction(1));
			assert.ok(!ape.isFunction(1.1));
			assert.ok(!ape.isFunction({}));
			assert.ok(!ape.isFunction(new Object()));
			assert.ok(ape.isFunction(function() {
			}));
			function a() {
			}

			assert.ok(!ape.isFunction(new a()));
			assert.ok(ape.isFunction(Error));
			assert.ok(!ape.isFunction(Error()));
			assert.ok(!ape.isFunction(new Error()));

			assert.ok(!ape.isFunction(new Number()));
			assert.ok(ape.isFunction(Number));
			assert.ok(!ape.isFunction(Number.prototype));

			assert.ok(!ape.isFunction([]));
			assert.ok(!ape.isFunction(new Array()));
			assert.ok(ape.isFunction(Array));
			assert.ok(!ape.isFunction(Array.prototype));

			assert.ok(!ape.isFunction("string"));
			assert.ok(!ape.isFunction(new String()));
			assert.ok(ape.isFunction(String));
			assert.ok(!ape.isFunction(String.prototype));

			assert.ok(ape.isFunction(RegExp));
			assert.ok(!ape.isFunction(RegExp()));
			assert.ok(!ape.isFunction(new RegExp()));
			assert.ok(!ape.isFunction(RegExp.prototype));
			assert.ok(!ape.isFunction(/\d/));
		});

		it("isNumber", function() {
			assert.ok(!ape.isNumber(new Date()));
			assert.ok(ape.isNumber(Number.NEGATIVE_INFINITY));
			assert.ok(ape.isNumber(Number.POSITIVE_INFINITY));
			assert.ok(ape.isNumber(Infinity));
			assert.ok(ape.isNumber(NaN));
			assert.ok(!ape.isNumber(null));
			assert.ok(!ape.isNumber(undefined));
			assert.ok(!ape.isNumber(true));
			assert.ok(!ape.isNumber(false));
			assert.ok(ape.isNumber(0));
			assert.ok(ape.isNumber(1));
			assert.ok(ape.isNumber(1.1));
			assert.ok(!ape.isNumber({}));
			assert.ok(!ape.isNumber(new Object()));
			assert.ok(!ape.isNumber(function() {
			}));
			function a() {
			}

			assert.ok(!ape.isNumber(new a()));
			assert.ok(!ape.isNumber(Error));
			assert.ok(!ape.isNumber(Error()));
			assert.ok(!ape.isNumber(new Error()));

			assert.ok(ape.isNumber(new Number()));
			assert.ok(!ape.isNumber(Number));
			assert.ok(ape.isNumber(Number.prototype));

			assert.ok(!ape.isNumber([]));
			assert.ok(!ape.isNumber(new Array()));
			assert.ok(!ape.isNumber(Array));
			assert.ok(!ape.isNumber(Array.prototype));

			assert.ok(!ape.isNumber("string"));
			assert.ok(!ape.isNumber(new String()));
			assert.ok(!ape.isNumber(String));
			assert.ok(!ape.isNumber(String.prototype));

			assert.ok(!ape.isNumber(RegExp));
			assert.ok(!ape.isNumber(RegExp()));
			assert.ok(!ape.isNumber(new RegExp()));
			assert.ok(!ape.isNumber(RegExp.prototype));
			assert.ok(!ape.isNumber(/\d/));
		});

		it("isDate", function() {
			assert.ok(ape.isDate(new Date()));
			assert.ok(!ape.isDate(Number.NEGATIVE_INFINITY));
			assert.ok(!ape.isDate(Number.POSITIVE_INFINITY));
			assert.ok(!ape.isDate(Infinity));
			assert.ok(!ape.isDate(NaN));
			assert.ok(!ape.isDate(null));
			assert.ok(!ape.isDate(undefined));
			assert.ok(!ape.isDate(true));
			assert.ok(!ape.isDate(false));
			assert.ok(!ape.isDate(0));
			assert.ok(!ape.isDate(1));
			assert.ok(!ape.isDate(1.1));
			assert.ok(!ape.isDate({}));
			assert.ok(!ape.isDate(new Object()));
			assert.ok(!ape.isDate(function() {
			}));
			function a() {
			}

			assert.ok(!ape.isDate(new a()));
			assert.ok(!ape.isDate(Error));
			assert.ok(!ape.isDate(Error()));
			assert.ok(!ape.isDate(new Error()));

			assert.ok(!ape.isDate(new Number()));
			assert.ok(!ape.isDate(Number));
			assert.ok(!ape.isDate(Number.prototype));

			assert.ok(!ape.isDate([]));
			assert.ok(!ape.isDate(new Array()));
			assert.ok(!ape.isDate(Array));
			assert.ok(!ape.isDate(Array.prototype));

			assert.ok(!ape.isDate("string"));
			assert.ok(!ape.isDate(new String()));
			assert.ok(!ape.isDate(String));
			assert.ok(!ape.isDate(String.prototype));

			assert.ok(!ape.isDate(RegExp));
			assert.ok(!ape.isDate(RegExp()));
			assert.ok(!ape.isDate(new RegExp()));
			assert.ok(!ape.isDate(RegExp.prototype));
			assert.ok(!ape.isDate(/\d/));
		});

		it("isNumeric", function() {
			var t = ape.isNumeric,
				ToString = function(value) {
					this.toString = function() {
						return String(value);
					};
				};
			assert.ok(t("-10"), "Negative integer string");
			assert.ok(t("0"), "Zero string");
			assert.ok(t("5"), "Positive integer string");
			assert.ok(t(-16), "Negative integer number");
			assert.ok(t(0), "Zero integer number");
			assert.ok(t(32), "Positive integer number");
			assert.ok(t("040"), "Octal integer literal string");
			assert.ok(t("0xFF"), "Hexadecimal integer literal string");
			assert.ok(t(0xFFF), "Hexadecimal integer literal");
			assert.ok(t("-1.6"), "Negative floating point string");
			assert.ok(t("4.536"), "Positive floating point string");
			assert.ok(t(-2.6), "Negative floating point number");
			assert.ok(t(3.1415), "Positive floating point number");
			assert.ok(t(1.5999999999999999), "Very precise floating point number");
			assert.ok(t(8e5), "Exponential notation");
			assert.ok(t("123e-2"), "Exponential notation string");
			assert.ok(t(new ToString("42")), "Custom .toString returning number");

			assert.equal(t(""), false, "Empty string");
			assert.equal(t("        "), false, "Whitespace characters string");
			assert.equal(t("\t\t"), false, "Tab characters string");
			assert.equal(t("abcdefghijklm1234567890"), false, "Alphanumeric character string");
			assert.equal(t("xabcdefx"), false, "Non-numeric character string");
			assert.equal(t(true), false, "Boolean true literal");
			assert.equal(t(false), false, "Boolean false literal");
			assert.equal(t("bcfed5.2"), false, "Number with preceding non-numeric characters");
			assert.equal(t("7.2acdgs"), false, "Number with trailling non-numeric characters");
			assert.equal(t(undefined), false, "Undefined value");
			assert.equal(t(null), false, "Null value");
			assert.equal(t(NaN), false, "NaN value");
			assert.equal(t(Infinity), false, "Infinity primitive");
			assert.equal(t(Number.POSITIVE_INFINITY), false, "Positive Infinity");
			assert.equal(t(Number.NEGATIVE_INFINITY), false, "Negative Infinity");
			assert.equal(t(new ToString("Devo")), false, "Custom .toString returning non-number");
			assert.equal(t({}), false, "Empty object");
			assert.equal(t(function() {
			}), false, "Instance of a function");
			assert.equal(t(new Date()), false, "Instance of a Date");
		});

		it("isRegExp", function() {
			assert.ok(!ape.isRegExp(new Date()));
			assert.ok(!ape.isRegExp(Number.NEGATIVE_INFINITY));
			assert.ok(!ape.isRegExp(Number.POSITIVE_INFINITY));
			assert.ok(!ape.isRegExp(Infinity));
			assert.ok(!ape.isRegExp(NaN));
			assert.ok(!ape.isRegExp(null));
			assert.ok(!ape.isRegExp(undefined));
			assert.ok(!ape.isRegExp(true));
			assert.ok(!ape.isRegExp(false));
			assert.ok(!ape.isRegExp(0));
			assert.ok(!ape.isRegExp(1));
			assert.ok(!ape.isRegExp(1.1));
			assert.ok(!ape.isRegExp({}));
			assert.ok(!ape.isRegExp(new Object()));
			assert.ok(!ape.isRegExp(function() {
			}));
			function a() {
			}

			assert.ok(!ape.isRegExp(new a()));
			assert.ok(!ape.isRegExp(Error));
			assert.ok(!ape.isRegExp(Error()));
			assert.ok(!ape.isRegExp(new Error()));

			assert.ok(!ape.isRegExp(new Number()));
			assert.ok(!ape.isRegExp(Number));
			assert.ok(!ape.isRegExp(Number.prototype));

			assert.ok(!ape.isRegExp([]));
			assert.ok(!ape.isRegExp(new Array()));
			assert.ok(!ape.isRegExp(Array));
			assert.ok(!ape.isRegExp(Array.prototype));

			assert.ok(!ape.isRegExp("string"));
			assert.ok(!ape.isRegExp(new String()));
			assert.ok(!ape.isRegExp(String));
			assert.ok(!ape.isRegExp(String.prototype));

			assert.ok(!ape.isRegExp(RegExp));
			assert.ok(ape.isRegExp(RegExp()));
			assert.ok(ape.isRegExp(new RegExp()));
			assert.ok(ape.isRegExp(RegExp.prototype));
			assert.ok(ape.isRegExp(/\d/));
		});

		it("isError", function() {
			assert.ok(!ape.isError(new Date()));
			assert.ok(!ape.isError(Number.NEGATIVE_INFINITY));
			assert.ok(!ape.isError(Number.POSITIVE_INFINITY));
			assert.ok(!ape.isError(Infinity));
			assert.ok(!ape.isError(NaN));
			assert.ok(!ape.isError(null));
			assert.ok(!ape.isError(undefined));
			assert.ok(!ape.isError(true));
			assert.ok(!ape.isError(false));
			assert.ok(!ape.isError(0));
			assert.ok(!ape.isError(1));
			assert.ok(!ape.isError(1.1));
			assert.ok(!ape.isError({}));
			assert.ok(!ape.isError(new Object()));
			assert.ok(!ape.isError(function() {
			}));
			function a() {
			}

			assert.ok(!ape.isError(new a()));
			assert.ok(!ape.isError(Error));
			assert.ok(ape.isError(Error()));
			assert.ok(ape.isError(new Error()));

			assert.ok(!ape.isError(new Number()));
			assert.ok(!ape.isError(Number));
			assert.ok(!ape.isError(Number.prototype));

			assert.ok(!ape.isError([]));
			assert.ok(!ape.isError(new Array()));
			assert.ok(!ape.isError(Array));
			assert.ok(!ape.isError(Array.prototype));

			assert.ok(!ape.isError("string"));
			assert.ok(!ape.isError(new String()));
			assert.ok(!ape.isError(String));
			assert.ok(!ape.isError(String.prototype));

			assert.ok(!ape.isError(RegExp));
			assert.ok(!ape.isError(RegExp()));
			assert.ok(!ape.isError(new RegExp()));
			assert.ok(!ape.isError(RegExp.prototype));
			assert.ok(!ape.isError(/\d/));
		});

		it("isEmpty", function() {
			assert.ok(ape.isEmpty(new Date()));
			var d = new Date();
			d.a = 1;
			assert.ok(!ape.isEmpty(d));
			assert.ok(ape.isEmpty(Number.NEGATIVE_INFINITY));
			assert.ok(ape.isEmpty(Number.POSITIVE_INFINITY));
			assert.ok(ape.isEmpty(Infinity));
			assert.ok(ape.isEmpty(NaN));
			assert.ok(ape.isEmpty(null));
			assert.ok(ape.isEmpty(undefined));
			assert.ok(ape.isEmpty(true));
			assert.ok(ape.isEmpty(false));
			assert.ok(ape.isEmpty(0));
			assert.ok(ape.isEmpty(1));
			assert.ok(ape.isEmpty(1.1));
			assert.ok(ape.isEmpty({}));
			assert.ok(ape.isEmpty(new Object()));
			assert.ok(ape.isEmpty(function() {
			}));
			function a() {
			}

			assert.ok(ape.isEmpty(new a()));
			assert.ok(!ape.isEmpty(Error));
			//assert.ok(ape.isEmpty(Error()));
			//assert.ok(ape.isEmpty(new Error()));

			assert.ok(ape.isEmpty(new Number()));
			assert.ok(ape.isEmpty(Number));
			assert.ok(ape.isEmpty(Number.prototype));

			assert.ok(ape.isEmpty([]));
			assert.ok(ape.isEmpty(new Array()));
			assert.ok(ape.isEmpty(Array));
			assert.ok(!ape.isEmpty(Array.prototype));

			//assert.ok(ape.isEmpty("string"));
			assert.ok(ape.isEmpty(new String()));
			assert.ok(ape.isEmpty(String));
			//assert.ok(ape.isEmpty(String.prototype));

			//assert.ok(!ape.isEmpty(RegExp));
			assert.ok(ape.isEmpty(RegExp()));
			assert.ok(ape.isEmpty(new RegExp()));
			assert.ok(ape.isEmpty(RegExp.prototype));
			assert.ok(ape.isEmpty(/\d/));
		});

	});

});
